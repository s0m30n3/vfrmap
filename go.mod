module github.com/lian/msfs2020-go

go 1.14

require (
	github.com/gorilla/websocket v1.4.2
	github.com/jteeuwen/go-bindata v3.0.7+incompatible // indirect
	golang.org/x/sys v0.0.0-20201020230747-6e5568b54d1a
)
