package main

//go:generate go-bindata -pkg main -o bindata.go -prefix html html

// build: GOOS=windows GOARCH=amd64 go build -o vfrmap.exe github.com/lian/msfs2020-go/vfrmap

// Ordinateur\HKEY_CURRENT_USER\SOFTWARE\Valve\Steam
// SteamPath
// steamapps\libraryfolders.vdf
// steamapps\appmanifest_1250410.acf
// steamapps\common\MicrosoftFlightSimulator\Packages\Official\Steam
// E:\SteamLibrary\steamapps\common\MicrosoftFlightSimulator\Packages\Official\Steam\asobo-bushtrip-nevada\Missions\Asobo\BushTrips\nevada
// missions\Asobo\BushTrips\nevada\Nevada.PLN

import (
	"encoding/json"
	"encoding/xml"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"
	"time"
	"unsafe"

	"github.com/lian/msfs2020-go/simconnect"
	"github.com/lian/msfs2020-go/vfrmap/html/leafletjs"
	"github.com/lian/msfs2020-go/vfrmap/websockets"
)

type Report struct {
	simconnect.RecvSimobjectDataByType
	Title         [256]byte `name:"TITLE"`
	Altitude      float64   `name:"INDICATED ALTITUDE" unit:"feet"` // PLANE ALTITUDE or PLANE ALT ABOVE GROUND
	Latitude      float64   `name:"PLANE LATITUDE" unit:"degrees"`
	Longitude     float64   `name:"PLANE LONGITUDE" unit:"degrees"`
	Heading       float64   `name:"PLANE HEADING DEGREES TRUE" unit:"degrees"`
	Airspeed      float64   `name:"AIRSPEED INDICATED" unit:"knot"`
	AirspeedTrue  float64   `name:"AIRSPEED TRUE" unit:"knot"`
	VerticalSpeed float64   `name:"VERTICAL SPEED" unit:"ft/min"`
	Flaps         float64   `name:"TRAILING EDGE FLAPS LEFT ANGLE" unit:"degrees"`
	Trim          float64   `name:"ELEVATOR TRIM PCT" unit:"percent"`
	RudderTrim    float64   `name:"RUDDER TRIM PCT" unit:"percent"`
	AileronTrim   float64   `name:"AILERON TRIM PCT" unit:"percent"`
	NextWPLat     float64   `name:"GPS WP NEXT LAT" unit:"degrees"`
	NextWPLon     float64   `name:"GPS WP NEXT LON" unit:"degrees"`
	NextWPAlt     float64   `name:"GPS WP NEXT ALT" unit:"feet"`
	//Heading     bool      `name:"GPS WP PREV VALID"`
	PrevWPLat float64 `name:"GPS WP PREV LAT" unit:"degrees"`
	PrevWPLon float64 `name:"GPS WP PREV LON" unit:"degrees"`
	PrevWPAlt float64 `name:"GPS WP PREV ALT" unit:"feet"`
}

func (r *Report) RequestData(s *simconnect.SimConnect) {
	defineID := s.GetDefineID(r)
	requestID := defineID
	s.RequestDataOnSimObjectType(requestID, defineID, 0, simconnect.SIMOBJECT_TYPE_USER)
}

type TrafficReport struct {
	simconnect.RecvSimobjectDataByType
	AtcID           [64]byte `name:"ATC ID"`
	AtcFlightNumber [8]byte  `name:"ATC FLIGHT NUMBER"`
	Altitude        float64  `name:"PLANE ALTITUDE" unit:"feet"`
	Latitude        float64  `name:"PLANE LATITUDE" unit:"degrees"`
	Longitude       float64  `name:"PLANE LONGITUDE" unit:"degrees"`
	Heading         float64  `name:"PLANE HEADING DEGREES TRUE" unit:"degrees"`
	NextWPLat       float64  `name:"GPS WP NEXT LAT" unit:"degrees"`
	NextWPLon       float64  `name:"GPS WP NEXT LON" unit:"degrees"`
	NextWPAlt       float64  `name:"GPS WP NEXT ALT" unit:"feet"`
	//Heading       bool     `name:"GPS WP PREV VALID"`
	PrevWPLat float64 `name:"GPS WP PREV LAT" unit:"degrees"`
	PrevWPLon float64 `name:"GPS WP PREV LON" unit:"degrees"`
	PrevWPAlt float64 `name:"GPS WP PREV ALT" unit:"feet"`
}

type AirportTimezone struct {
	Name   string
	Offset int
}

type AirportTime struct {
	Sunrise time.Time
	Sunset  time.Time
	Dawn    time.Time
	Dusk    time.Time
}

type AirportRunwayEndData struct {
	Ident string
	Lat   float64
	Lon   float64
}

type AirportRunwayNavaidsData struct {
	Ident     string
	Type      string
	Lat       float64
	Lon       float64
	Airport   int64
	Runway    string
	Frequency float64
	Slope     float64
	Bearing   float64
	Name      string
	Elevation float64
	Range     float64
}

type AirportRunwayData struct {
	Ident           string
	Width           float64
	Length          float64
	Bearing         float64
	Surface         string
	Markings        []string
	Lighting        []string
	ThresholdOffset float64
	OverrunLength   float64
	Ends            []AirportRunwayEndData
	Navaids         []AirportRunwayNavaidsData
}

type AirportFequencyData struct {
	Type      string
	frequency float64
	name      string
}

type AirportWeatherData struct {
	METAR string
	TAF   string
}

type AirportData struct {
	ICAO              string
	IATA              string
	Name              string
	RegionName        string
	Elevation         float64
	Lat               float64
	Lon               float64
	MagneticVariation float64
	Timezone          AirportTimezone
	Time              AirportTime
	RunwayCount       int
	Runways           []AirportRunwayData
	Frequencies       []AirportFequencyData
	Weather           AirportWeatherData
}

func (r *TrafficReport) RequestData(s *simconnect.SimConnect) {
	defineID := s.GetDefineID(r)
	requestID := defineID
	s.RequestDataOnSimObjectType(requestID, defineID, 0, simconnect.SIMOBJECT_TYPE_AIRCRAFT)
}

func (r *TrafficReport) Inspect() string {
	return fmt.Sprintf(
		"%s GPS %.6f %.6f @ %.0f feet %.0f°",
		r.AtcID,
		r.Latitude,
		r.Longitude,
		r.Altitude,
		r.Heading,
	)
}

type TeleportRequest struct {
	simconnect.RecvSimobjectDataByType
	Latitude  float64 `name:"PLANE LATITUDE" unit:"degrees"`
	Longitude float64 `name:"PLANE LONGITUDE" unit:"degrees"`
	Altitude  float64 `name:"PLANE ALTITUDE" unit:"feet"`
}

func (r *TeleportRequest) SetData(s *simconnect.SimConnect) {
	defineID := s.GetDefineID(r)

	buf := [3]float64{
		r.Latitude,
		r.Longitude,
		r.Altitude,
	}

	size := simconnect.DWORD(3 * 8) // 2 * 8 bytes
	s.SetDataOnSimObject(defineID, simconnect.OBJECT_ID_USER, 0, 0, size, unsafe.Pointer(&buf[0]))
}

type FlightPlanDoc struct {
	XMLName xml.Name   `xml:"SimBase.Document"`
	Descr   string     `xml:"Descr"`
	Flight  FlightPlan `xml:"FlightPlan.FlightPlan"`
}

func (f *FlightPlanDoc) Clear() {
	f.Flight = FlightPlan{}
	f.Descr = ""
	f.XMLName = xml.Name{}
}

type LongLatAlt struct {
	Longitude float64
	Latitude  float64
	Altitude  float64
}

func (o *LongLatAlt) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var content string
	if err := d.DecodeElement(&content, &start); err != nil {
		return err
	}

	var north byte
	var west byte
	var northDegree float64
	var northMinute float64
	var northSeconde float64
	var westDegree float64
	var westMinute float64
	var westSeconde float64
	var altitude float64

	fmt.Sscanf(content, "%c%v° %v' %v\",%c%v° %v' %v\",%v", &north, &northDegree, &northMinute, &northSeconde, &west, &westDegree, &westMinute, &westSeconde, &altitude)
	o.Latitude = northDegree + northMinute/60 + northSeconde/3600
	if north != 'N' {
		o.Latitude = -o.Latitude
	}
	o.Longitude = westDegree + westMinute/60 + westSeconde/3600
	if west != 'E' {
		o.Longitude = -o.Longitude
	}
	o.Altitude = altitude

	return nil
}

type FlightPlan struct {
	XMLName         xml.Name   `xml:"FlightPlan.FlightPlan"`
	Title           string     `xml:"Title"`
	FPType          string     `xml:"FPType"`
	RouteType       string     `xml:"RouteType"`
	CruisingAlt     float32    `xml:"CruisingAlt"`
	DepartureID     string     `xml:"DepartureID"`
	DepartureLLA    LongLatAlt `xml:"DepartureLLA"`
	DestinationID   string     `xml:"DestinationID"`
	DestinationLLA  LongLatAlt `xml:"DestinationLLA"`
	Descr           string     `xml:"Descr"`
	DepartureName   string     `xml:"DepartureName"`
	DestinationName string     `xml:"DestinationName"`
	/*<AppVersion>
	    <AppVersionMajor>11</AppVersionMajor>
	    <AppVersionBuild>282174</AppVersionBuild>
	</AppVersion>*/
	ATCWaypoint []Waypoint `xml:"ATCWaypoint"`
	Changed     bool
}

type ICAOStruct struct {
	XMLName   xml.Name `xml:"ICAO"`
	ICAOIdent string   `xml:"ICAOIdent"`
}

type Waypoint struct {
	XMLName         xml.Name   `xml:"ATCWaypoint"`
	ID              string     `xml:"id,attr"`
	ATCWaypointType string     `xml:"ATCWaypointType"`
	WorldPosition   LongLatAlt `xml:"WorldPosition"`
	SpeedMaxFP      float32    `xml:"SpeedMaxFP"`
	ICAO            ICAOStruct `xml:"ICAO"`
	Airportdata     AirportData
	/*<ICAO>
	    <ICAOIdent>SBGL</ICAOIdent>
	</ICAO>*/
}

func MustAsset(name string) []byte {
	asset, err := Asset(name)
	if err != nil {
		log.Fatalf("Could not load asset %s", name)
	}

	return asset
}

var buildVersion string
var buildTime string
var disableTeleport bool

var fsPath string

var verbose bool
var httpListen string
var flightPlan *FlightPlan
var lastFlightPlanDoc FlightPlanDoc

var eventSimStartID simconnect.DWORD
var startupTextEventID simconnect.DWORD
var flightPlanActivatedID simconnect.DWORD
var flightPlanDeactivatedID simconnect.DWORD

var report *Report
var trafficReport *TrafficReport
var teleportReport *TeleportRequest

func subscribeToEvents(s *simconnect.SimConnect) {
	report = &Report{}
	err := s.RegisterDataDefinition(report)
	if err != nil {
		panic(err)
	}

	trafficReport = &TrafficReport{}
	err = s.RegisterDataDefinition(trafficReport)
	if err != nil {
		panic(err)
	}

	teleportReport = &TeleportRequest{}
	err = s.RegisterDataDefinition(teleportReport)
	if err != nil {
		panic(err)
	}

	eventSimStartID = s.GetEventID()
	//s.SubscribeToSystemEvent(eventSimStartID, "SimStart")
	//s.SubscribeToFacilities(simconnect.FACILITY_LIST_TYPE_AIRPORT, s.GetDefineID(&simconnect.DataFacilityAirport{}))
	//s.SubscribeToFacilities(simconnect.FACILITY_LIST_TYPE_WAYPOINT, s.GetDefineID(&simconnect.DataFacilityWaypoint{}))

	startupTextEventID = s.GetEventID()
	s.ShowText(simconnect.TEXT_TYPE_PRINT_WHITE, 15, startupTextEventID, "msfs2020-go/vfrmap connected")

	flightPlanActivatedID = s.GetEventID()
	s.SubscribeToSystemEvent(flightPlanActivatedID, "FlightPlanActivated")

	flightPlanDeactivatedID = s.GetEventID()
	s.SubscribeToSystemEvent(flightPlanDeactivatedID, "FlightPlanDeactivated")
}

func getFSDir() string {
	/*k, err := registry.OpenKey(registry.CURRENT_USER, `SOFTWARE\Valve\Steam`, registry.QUERY_VALUE)
	if err != nil {
		log.Fatal(err)
	}
	defer k.Close()

	steamPath, _, err := k.GetStringValue("SteamPath")
	if err != nil {
		log.Fatal(err)
	}

	steamAppPaths := make([]string, 0)
	steamAppPaths = append(steamAppPaths, steamPath+"/steamapps")

	{
		libFolder, err := os.OpenFile(steamPath+"/steamapps/libraryfolders.vdf", os.O_RDONLY, 0)
		if err != nil {
			fmt.Println(err)
			return ""
		}
		defer libFolder.Close()

		byteValue, _ := ioutil.ReadAll(libFolder)
		re := regexp.MustCompile(`[ \t]*\"[0-9]+\"[ \t]+\"([a-zA-Z0-1:\\]+)\"[ \t]*`)
		matchs := re.FindAllSubmatch(byteValue, -1)
		if matchs != nil {
			for _, match := range matchs {
				if len(match) > 1 {
					steamAppPaths = append(steamAppPaths, string(match[1])+"/steamapps")
				}
			}
		}
	}

	for _, steamAppPath := range steamAppPaths {
		stringFileName := steamAppPath + "/appmanifest_1250410.acf"
		_, err = os.Stat(stringFileName)
		if err == nil {
			return steamAppPath + "/common/MicrosoftFlightSimulator"
		}
	}

	return ""*/

	configFile, err := os.OpenFile(os.Getenv("APPDATA")+"/Microsoft Flight Simulator/UserCfg.opt", os.O_RDONLY, 0)
	if err != nil {
		fmt.Println(err)
		return ""
	}
	defer configFile.Close()

	byteValue, _ := ioutil.ReadAll(configFile)
	re := regexp.MustCompile(`[ \t]*InstalledPackagesPath[ \t]+\"([a-zA-Z0-1:\\]+)\"[ \t]*`)
	matchs := re.FindAllSubmatch(byteValue, -1)
	if matchs != nil {
		for _, match := range matchs {
			if len(match) > 1 {
				return string(match[1])
			}
		}
	}

	return ""
}

func CaseInsensitiveContains(s, substr string) bool {
	s, substr = strings.ToUpper(s), strings.ToUpper(substr)
	return strings.Contains(s, substr)
}

func findFlightPlanInFSPath(flightPlanName string) string {
	packagePath := fsPath
	finalFlightPlanPath := ""

	err := filepath.Walk(packagePath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if CaseInsensitiveContains(path, flightPlanName) {
				finalFlightPlanPath = path
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}

	return finalFlightPlanPath
}

func main() {
	flag.BoolVar(&verbose, "verbose", false, "verbose output")
	flag.StringVar(&httpListen, "listen", "0.0.0.0:9000", "http listen")
	flag.BoolVar(&disableTeleport, "disable-teleport", false, "disable teleport")
	flag.Parse()

	fmt.Printf("\nmsfs2020-go/vfrmap\n  readme: https://github.com/lian/msfs2020-go/blob/master/vfrmap/README.md\n  issues: https://github.com/lian/msfs2020-go/issues\n  version: %s (%s)\n\n", buildVersion, buildTime)

	exitSignal := make(chan os.Signal, 1)
	signal.Notify(exitSignal, os.Interrupt, syscall.SIGTERM)
	exePath, _ := os.Executable()

	fsPath = getFSDir()
	fmt.Printf("msf path : " + fsPath + "\n")

	ws := websockets.New()

	s, err := simconnect.New("msfs2020-go/vfrmap")
	if err != nil {
		panic(err)
	}

	s.TryConnect()
	fmt.Println("connected to flight simulator!")

	subscribeToEvents(s)

	go func() {
		app := func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Access-Control-Allow-Origin", "*")
			w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
			w.Header().Set("Pragma", "no-cache")
			w.Header().Set("Expires", "0")
			w.Header().Set("Content-Type", "text/html")

			filePath := filepath.Join(filepath.Dir(exePath), "index.html")

			if _, err = os.Stat(filePath); os.IsNotExist(err) {
				w.Write(MustAsset(filepath.Base(filePath)))
			} else {
				fmt.Println("use local", filePath)
				http.ServeFile(w, r, filePath)
			}
		}

		http.HandleFunc("/ws", ws.Serve)
		http.Handle("/leafletjs/", http.StripPrefix("/leafletjs/", leafletjs.FS{}))
		http.HandleFunc("/", app)
		//http.Handle("/", http.FileServer(http.Dir(".")))

		err := http.ListenAndServe(httpListen, nil)
		if err != nil {
			panic(err)
		}
	}()

	simconnectTick := time.NewTicker(100 * time.Millisecond)
	planePositionTick := time.NewTicker(200 * time.Millisecond)
	trafficPositionTick := time.NewTicker(10000 * time.Millisecond)

	for {
		select {
		case <-planePositionTick.C:
			report.RequestData(s)

		case <-trafficPositionTick.C:
			//fmt.Println("--------------------------------- REQUEST TRAFFIC --------------")
			//trafficReport.RequestData(s)
			//s.RequestFacilitiesList(simconnect.FACILITY_LIST_TYPE_AIRPORT, airportRequestID)
			//s.RequestFacilitiesList(simconnect.FACILITY_LIST_TYPE_WAYPOINT, waypointRequestID)

		case <-simconnectTick.C:
			ppData, r1, err := s.GetNextDispatch()

			if r1 < 0 {
				if uint32(r1) == simconnect.E_FAIL {
					// skip error, means no new messages?
					continue
				} else {
					fmt.Errorf("GetNextDispatch error: %d %s", r1, err)
					//panic(fmt.Errorf("GetNextDispatch error: %d %s", r1, err))
					s.TryConnect()
					subscribeToEvents(s)
					continue
				}
			}

			recvInfo := *(*simconnect.Recv)(ppData)

			switch recvInfo.ID {
			case simconnect.RECV_ID_EXCEPTION:
				recvErr := *(*simconnect.RecvException)(ppData)
				fmt.Printf("SIMCONNECT_RECV_ID_EXCEPTION %#v\n", recvErr)

			case simconnect.RECV_ID_OPEN:
				recvOpen := *(*simconnect.RecvOpen)(ppData)
				fmt.Printf(
					"\nflight simulator info:\n  codename: %s\n  version: %d.%d (%d.%d)\n  simconnect: %d.%d (%d.%d)\n\n",
					recvOpen.ApplicationName,
					recvOpen.ApplicationVersionMajor,
					recvOpen.ApplicationVersionMinor,
					recvOpen.ApplicationBuildMajor,
					recvOpen.ApplicationBuildMinor,
					recvOpen.SimConnectVersionMajor,
					recvOpen.SimConnectVersionMinor,
					recvOpen.SimConnectBuildMajor,
					recvOpen.SimConnectBuildMinor,
				)

			case simconnect.RECV_ID_EVENT:
				recvEvent := *(*simconnect.RecvEvent)(ppData)

				switch recvEvent.EventID {
				case eventSimStartID:
					fmt.Println("EVENT: SimStart")
				case startupTextEventID:
					// ignore
				case flightPlanActivatedID:
					// ignore
				case flightPlanDeactivatedID:
					fmt.Println("Flightplan deactivated")
					//flightPlan = nil
				default:
					fmt.Println("unknown SIMCONNECT_RECV_ID_EVENT", recvEvent.EventID)
				}
			case simconnect.RECV_ID_WAYPOINT_LIST:
				waypointList := *(*simconnect.RecvFacilityWaypointList)(ppData)
				fmt.Printf("SIMCONNECT_RECV_ID_WAYPOINT_LIST %#v\n", waypointList)

			case simconnect.RECV_ID_AIRPORT_LIST:
				airportList := *(*simconnect.RecvFacilityAirportList)(ppData)
				fmt.Printf("SIMCONNECT_RECV_ID_AIRPORT_LIST %#v\n", airportList)

			case simconnect.RECV_ID_SIMOBJECT_DATA_BYTYPE:
				recvData := *(*simconnect.RecvSimobjectDataByType)(ppData)

				switch recvData.RequestID {
				case s.DefineMap["Report"]:
					report = (*Report)(ppData)

					if verbose {
						fmt.Printf("REPORT: %#v\n", report)
					}

					ws.Broadcast(map[string]interface{}{
						"type":           "plane",
						"latitude":       report.Latitude,
						"longitude":      report.Longitude,
						"altitude":       fmt.Sprintf("%.0f", report.Altitude),
						"heading":        int(report.Heading),
						"airspeed":       fmt.Sprintf("%.0f", report.Airspeed),
						"airspeed_true":  fmt.Sprintf("%.0f", report.AirspeedTrue),
						"vertical_speed": fmt.Sprintf("%.0f", report.VerticalSpeed),
						"flaps":          fmt.Sprintf("%.0f", report.Flaps),
						"trim":           fmt.Sprintf("%.1f", report.Trim),
						"rudder_trim":    fmt.Sprintf("%.1f", report.RudderTrim),
						"aileron_trim":   fmt.Sprintf("%.1f", report.AileronTrim),
						"NextWPLat":      report.NextWPLat,
						"NextWPLon":      report.NextWPLon,
						"NextWPAlt":      fmt.Sprintf("%.0f", report.NextWPAlt),
						"PrevWPLat":      report.PrevWPLat,
						"PrevWPLon":      report.PrevWPLon,
						"PrevWPAlt":      fmt.Sprintf("%.0f", report.PrevWPAlt),
						"Plan":           flightPlan,
					})

					if flightPlan != nil {
						flightPlan.Changed = false
					}

				case s.DefineMap["TrafficReport"]:
					trafficReport = (*TrafficReport)(ppData)
					fmt.Printf("TRAFFIC REPORT: %s\n", trafficReport.Inspect())
				}

			case simconnect.RECV_ID_EVENT_FILENAME:
				recvFilename := *(*simconnect.RecvFilename)(ppData)
				stringFileName := string(recvFilename.FileName[:])
				if stringFileName != "" {
					stringFileName = strings.Trim(stringFileName, "\x00")
					fmt.Println("flightPlanActivatedID", stringFileName)
					_, err = os.Stat(stringFileName)
					if err == nil {
						readFlightPlan(stringFileName)
					} else if stringFileName != ".PLN" {
						stringFileName = findFlightPlanInFSPath(stringFileName)
						if stringFileName != "" {
							readFlightPlan(stringFileName)
						}
					}
				}

			default:
				fmt.Println("recvInfo.ID unknown", recvInfo.ID)
			}

		case <-exitSignal:
			fmt.Println("exiting..")
			if err = s.Close(); err != nil {
				panic(err)
			}
			os.Exit(0)

		case _ = <-ws.NewConnection:
			// drain and skip

		case m := <-ws.ReceiveMessages:
			handleClientMessage(m, s)
		}
	}
}

func handleClientMessage(m websockets.ReceiveMessage, s *simconnect.SimConnect) {
	var pkt map[string]interface{}
	if err := json.Unmarshal(m.Message, &pkt); err != nil {
		fmt.Println("invalid websocket packet", err)
	} else {
		pktType, ok := pkt["type"].(string)
		if !ok {
			fmt.Println("invalid websocket packet", pkt)
			return
		}
		switch pktType {
		case "teleport":
			if disableTeleport {
				fmt.Println("teleport disabled", pkt)
				return
			}

			// validate user input
			lat, ok := pkt["lat"].(float64)
			if !ok {
				fmt.Println("invalid websocket packet", pkt)
				return
			}
			lng, ok := pkt["lng"].(float64)
			if !ok {
				fmt.Println("invalid websocket packet", pkt)
				return
			}
			altitude, ok := pkt["altitude"].(float64)
			if !ok {
				fmt.Println("invalid websocket packet", pkt)
				return
			}

			// teleport
			r := &TeleportRequest{Latitude: lat, Longitude: lng, Altitude: altitude}
			r.SetData(s)
		}
	}
}

func readFlightPlan(filePath string) {
	xmlFile, err := os.OpenFile(filePath, os.O_RDONLY, 0)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer xmlFile.Close()

	byteValue, _ := ioutil.ReadAll(xmlFile)

	lastFlightPlanDoc.Clear()
	xml.Unmarshal(byteValue, &lastFlightPlanDoc)

	flightPlan = &(lastFlightPlanDoc.Flight)

	client := &http.Client{}
	for i := range flightPlan.ATCWaypoint {
		if flightPlan.ATCWaypoint[i].ATCWaypointType == "Airport" {
			url := "https://api.flightplandatabase.com/nav/airport/" + flightPlan.ATCWaypoint[i].ICAO.ICAOIdent
			req, err := http.NewRequest("GET", url, nil)
			if err != nil {
				continue
			}

			req.SetBasicAuth("5Irb2qUCxj2VUP1Ryj9LDQaaycZ6U3MX0yHB5MB8", "")
			resp, err := client.Do(req)
			if err != nil {
				continue
			}

			defer resp.Body.Close()
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				continue
			}

			err = json.Unmarshal(body, &flightPlan.ATCWaypoint[i].Airportdata)
			if err != nil {
				fmt.Println(err)
				continue
			}
		}
	}
	flightPlan.Changed = true
}
